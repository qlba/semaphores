#include "addon-api.h"

using namespace Napi;

void throwError(Env env, const char *error)
{
	throw Error::New(env, error);
}
