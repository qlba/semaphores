#ifndef NAPI
#define NAPI

#include <napi.h>

#define ENV info.Env()

void throwError(Napi::Env env, const char *error);

#endif // NAPI
