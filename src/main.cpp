#include <semaphore.h>
#include "addon-api.h"

using namespace Napi;

#define ASSERT(condition) if (!(condition)) throwError(ENV, strerror(errno));

void init(const CallbackInfo &info)
{
	int result = sem_init(
		info[0].As<Buffer<sem_t>>().Data(),
		info[1].As<Number>().Int32Value(),
		info[2].As<Number>().Uint32Value()
	);

	ASSERT(result == 0)
}

void destroy(const CallbackInfo &info)
{
	int result = sem_destroy(
		info[0].As<Buffer<sem_t>>().Data()
	);

	ASSERT(result == 0)
}

Value open(const CallbackInfo &info)
{
	sem_t *sem = sem_open(
		info[0].As<String>().Utf8Value().c_str(),
		info[1].As<Number>().Int32Value(),
		info[2].As<Number>().Uint32Value(),
		info[3].As<Number>().Uint32Value()
	);
	
	ASSERT(sem != NULL)

	return Buffer<sem_t>::New(ENV, sem, sizeof(sem_t *));
}

void close(const CallbackInfo &info)
{
	int result = sem_close(
		info[0].As<Buffer<sem_t>>().Data()
	);

	ASSERT(result == 0)
}

void unlink(const CallbackInfo &info)
{
	int result = sem_unlink(
		info[0].As<String>().Utf8Value().c_str()
	);

	ASSERT(result == 0)
}

void wait(const CallbackInfo &info)
{
	int result = sem_wait(
		info[0].As<Buffer<sem_t>>().Data()
	);

	ASSERT(result == 0)
}

#ifdef __USE_XOPEN2K

void timedwait(const CallbackInfo &info)
{
	timespec abstime;

	abstime.tv_sec = info[1].As<Number>().Int64Value();
	abstime.tv_nsec = info[2].As<Number>().Int64Value();

	int result = sem_timedwait(
		info[0].As<Buffer<sem_t>>().Data(),
		&abstime
	);

	ASSERT(result == 0)
}

#endif

void trywait(const CallbackInfo &info)
{
	int result = sem_trywait(
		info[0].As<Buffer<sem_t>>().Data()
	);

	ASSERT(result == 0)
}

void post(const CallbackInfo &info)
{
	int result = sem_post(
		info[0].As<Buffer<sem_t>>().Data()
	);

	ASSERT(result == 0)
}

Value getvalue(const CallbackInfo &info)
{
	int sval;

	int result = sem_getvalue(
		info[0].As<Buffer<sem_t>>().Data(),
		&sval
	);

	ASSERT(result == 0)

	return Number::From(ENV, sval);
}



Object Init(Env env, Object exports)
{
	exports.Set("sem_init", Function::New(env, init));
	exports.Set("sem_destroy", Function::New(env, destroy));
	exports.Set("sem_open", Function::New(env, open));
	exports.Set("sem_close", Function::New(env, close));
	exports.Set("sem_unlink", Function::New(env, unlink));
	exports.Set("sem_wait", Function::New(env, wait));

#	ifdef __USE_XOPEN2K

	exports.Set("sem_timedwait", Function::New(env, timedwait));

#	endif

	exports.Set("sem_trywait", Function::New(env, trywait));
	exports.Set("sem_post", Function::New(env, post));
	exports.Set("sem_getvalue", Function::New(env, getvalue));

	return exports;
}

NODE_API_MODULE(hello, Init)
